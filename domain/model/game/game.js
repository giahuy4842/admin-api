var name;
var check_staff_code;
var started_date;
var ended_date;
var url;
var type;
var department;
var status;
var created_user_id;

function Game(
	name,
	check_staff_code,
	started_date,
	ended_date,
	url,
	type,
	department,
	status,
	created_user_id
) {
	this.name = name;
	this.check_staff_code = check_staff_code;
	this.started_date = started_date;
	this.ended_date = ended_date;
	this.url = url;
	this.type = type;
	this.department = department;
	this.status = status;
	this.created_user_id = created_user_id;
}

module.exports = Game;
