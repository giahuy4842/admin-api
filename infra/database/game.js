var mysqlConnection = require("./mysql").getConnection();

function populateCondition(filter) {
	var conditions = " ";
	for (var key in filter) {
		var value = filter[key];
		if (typeof value === "object") {
			if (value.type === "string") {
				conditions =
					conditions +
					" AND " +
					value.field +
					" " +
					value.operator +
					" " +
					"'" +
					value.value +
					"'";
			} else {
				conditions =
					conditions +
					" AND " +
					value.field +
					" " +
					value.operator +
					" " +
					value.value;
			}
		} else if (typeof value === "string") {
			conditions = conditions + " AND " + key + " = " + "'" + value + "'";
		} else {
			conditions = conditions + " AND " + key + " = " + value;
		}
	}

	return conditions;
}

async function getById(gameId) {
	const promisePool = mysqlConnection.promise();
	var [results] = await promisePool.query(
		"" + "SELECT * FROM games WHERE id = ?",
		[gameId]
	);
	return results[0];
}

async function getGames(filter, options, whereParams) {
	var limit = options.limit ? options.limit : 10;
	var skip = 0,
		offset = 0,
		page = 1;

	if (options.offset) {
		offset = options.offset;
		skip = offset;
	} else if (options.page) {
		page = options.page;
		skip = (page - 1) * limit;
	}

	var conditions = populateCondition(filter);

	const promisePool = mysqlConnection.promise();

	var [results] = await promisePool.query(
		"SELECT count(id) as numRows FROM games WHERE 1 = 1 AND deleted_date IS NULL " +
			conditions
	);
	var numRows = results[0].numRows;
	var offsetLimit = skip + "," + limit;

	if (options.select == "" || !options.select) {
		var select =
			"" +
			"SELECT id, name, created_date, started_date, ended_date, url, type, department, status, updated_date, created_user_id, updated_user_id " +
			"FROM games ";
	} else {
		var select = "" + "SELECT " + options.select + " " + "FROM games ";
	}

	var params = [];

	if (whereParams.length > 0) {
		select += " WHERE ";
		for (let key in whereParams) {
			if (key !== "0") {
				select += " AND ";
			}
			select += whereParams[key].field + " " + whereParams[key].condition;
			params.push(whereParams[key].value);
		}
	} else {
		select += "WHERE 1 = 1 ";
	}

	select += " AND deleted_date IS NULL " + conditions;

	if (options.orderBy) {
		select += " ORDER BY " + options.orderBy;
	}

	select += " LIMIT " + offsetLimit;

	[results] = await promisePool.query(select, params);

	return {
		list: results,
		total: numRows,
		limit: limit,
		offset: offset,
	};
}

async function createGame(game) {
	const promisePool = mysqlConnection.promise();
	var [results] = await promisePool.query("INSERT INTO games SET ?", game);
	return results.insertId;
}

async function updateGame(game) {
	const promisePool = mysqlConnection.promise();
	try {
		var mysql = mysqlConnection.format(
			"" +
				"UPDATE " +
				"games SET name = ?, started_date = ?, ended_date = ?, " +
				"url = ?, type = ?, department = ?, status = ?, updated_user_id = ?, " +
				"updated_date = NOW() " +
				"WHERE id = ?; ",
			[
				game.name,
				game.started_date,
				game.ended_date,
				game.url,
				game.type,
				game.department,
				game.status,
				game.updated_user_id,
				game.id,
			]
		);
		var [results] = await promisePool.query(mysql);
		return results.changedRows;
	} catch (err) {
		throw new Error(err);
	}
}

async function deleteGame(gameId, whereParams) {
	const promisePool = mysqlConnection.promise();

	try {
		var params;
		if (whereParams.length > 0) {
			for (let key in whereParams) {
				if ((whereParams[key].field = "user_id")) {
					params = whereParams[key].value;
				}
			}
		}

		var mysql = mysqlConnection.format(
			"" +
				"UPDATE games SET updated_user_id = ?, deleted_user_id = ?, updated_date = NOW(), deleted_date = NOW() WHERE id = ? ",
			[params, params, gameId]
		);
		var [results] = await promisePool.query(mysql);
		return results;
	} catch (err) {
		throw new Error(err);
	}
}

async function getFormsByGameId(gameId) {
	const promisePool = mysqlConnection.promise();

	try {
		var mysqlGetGame = mysqlConnection.format(
			" " +
				"SELECT games.id, name, started_date, ended_date, url, type, status, check_staff_code " +
				"FROM games " +
				"WHERE games.id = ? ",
			[gameId]
		);

		var mysqlGetForm = mysqlConnection.format(
			" " +
				"SELECT forms.id as forms_id, form, crm_info, type as form_type, form_selector.id as form_selector_id, form_selector.input_selector " +
				"FROM forms " +
				"LEFT JOIN form_selector ON forms.id = form_selector.form_id " +
				"WHERE forms.game_id = ? ",
			[gameId]
		);

		var [gameResult] = await promisePool.query(mysqlGetGame);
		var [formsResult] = await promisePool.query(mysqlGetForm);

		var gameStateResult = [],
			index = {};
		gameResult.forEach((item) => {
			index[item.id] = {
				id: item.id,
				name: item.name,
				started_date: item.started_date,
				ended_date: item.ended_date,
				url: item.url,
				type: item.type,
				status: item.status,
				check_staff_code: item.check_staff_code,
				forms: [],
			};
			gameStateResult.push(index[item.id]);
		});

		formsResult.forEach((item) => {
			gameStateResult[0].forms.push(item);
		});

		return gameStateResult[0];
	} catch (err) {
		throw new Error(err);
	}
}

async function getListGames(filter) {
	var conditions = populateCondition(filter);
	const promisePool = mysqlConnection.promise();

	try {
		var mysql = mysqlConnection.format(
			"" +
				"SELECT id, name, created_date, started_date, ended_date, url, type, department, status, updated_date " +
				"FROM games WHERE 1 = 1 AND deleted_date IS NULL " +
				conditions
		);
		var [results] = await promisePool.query(mysql);
		return results;
	} catch (err) {
		throw new Error(err);
	}
}

module.exports = {
	getById: getById,
	getGames: getGames,
	createGame: createGame,
	updateGame: updateGame,
	deleteGame: deleteGame,
	getFormsByGameId: getFormsByGameId,
	getListGames: getListGames,
};
