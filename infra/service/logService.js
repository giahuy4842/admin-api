var winston = require('../logging/winston');
var axios = require("axios");
var notificationService = require('./notification');

async function getLogsApi(options, filter) {

    var url = 'https://thammyvienngocdung.com/wp-json/log/search';
    var errText = '';
    var fromDate, toDate, keyword;
    try {
        for (var key in filter) {
            var value = filter[key];
            if (key === "from_date") {
                fromDate = value.value;
            }

            if (key === "to_date") {
                toDate = value.value;
            }

            if (key === "key") {
                keyword = value.value;
            }
        }

        var response = await axios
            .get(url, {
                headers: {
                    'Content-Type': 'application/json',
                },
                timeout: 10000,
                params: {
                    offset: options.offset,
                    limit: options.limit,
                    from_date: fromDate,
                    to_date: toDate,
                    key: keyword,
                }
            });

        return response.data.result;

    } catch (err) {
        errText = 'getLogsApi error url = ' + url + ' err = ' + err.message;
        notificationService.sendByTelegram(encodeURIComponent(errText));
        winston.error(errText);
        return [];
    }
}

module.exports = {
    getLogsApi: getLogsApi,
}